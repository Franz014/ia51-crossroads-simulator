package views;

import io.sarl.lang.annotation.SarlElementType;
import io.sarl.lang.annotation.SarlSpecification;
import io.sarl.lang.annotation.SyntheticMember;

@SarlSpecification("0.11")
@SarlElementType(10)
@SuppressWarnings("all")
public class MainFrame {
  @SyntheticMember
  public MainFrame() {
    super();
  }
}
