package crossroad_simulator;

import java.io.IOException;
import java.net.URL;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.layout.BorderPane;
import javafx.scene.Scene;

public class CrossroadSimulator extends Application 
{
	private static final int WIDTH = 640;
	private static final int HEIGHT = 480;
	private static final String TITLE = "Crossroad simulator";
	
	public void start (Stage primaryStage) throws Exception 
	{
		try
		{
			final URL url = this.getClass().getResource("./views/MainFrame.fxml");
			
			final FXMLLoader fxmlLoader = new FXMLLoader(url);
			
			final Object root = fxmlLoader.load();
			
			final Scene scene = new Scene((BorderPane) root, CrossroadSimulator.WIDTH, CrossroadSimulator.HEIGHT);
			
			primaryStage.setTitle(CrossroadSimulator.TITLE);
	        primaryStage.setScene(scene);
	        primaryStage.show();
		}
		catch (IOException ex)
		{
			ex.printStackTrace();
		}        
	}
	
	public static void main (String[] args) 
	{
		CrossroadSimulator.launch(args);
    }
	
}
